#ifndef INCLUDES_H
#define INCLUDES_H

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

typedef float td_p_reg;
typedef int td_coordinate;

enum R_Node_type {LEAVES, NODES};

#endif // INCLUDES_H
