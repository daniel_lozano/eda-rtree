#ifndef _RNODE_H
#define _RNODE_H

#include "includes.h"

template<typename C>
class _RNode
{
    public:
        typedef _RNode<C> RNode;
        typedef typename C::T T;
        typedef typename C::RFrame RFrame;        

    private:
        T __data;
        pair<RFrame *, T> * __p_leaves = nullptr;
        pair<RFrame *, RNode *> * __p_nodes = nullptr;
        R_Node_type __type = LEAVES;
        size_t __size = 0,
               __level = 0;

    public:
        _RNode()
        {

        }

        _RNode(size_t __size, R_Node_type __type)
        {
            this->__size = __size;
            this->__type = __type;

            if(this->__type == LEAVES)
                __p_leaves = new pair<RFrame * , T> [this->__size];
            else
                __p_nodes = new pair<RFrame *, RNode *>[this->__size];
        }

        size_t __f_get_size()
        {
            return this->__size;
        }

        R_Node_type __f_get_type()
        {
            return this->__type;
        }


};

#endif // _RNODE_H
