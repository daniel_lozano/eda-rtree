#ifndef _RTREE_H
#define _RTREE_H

#include "includes.h"
#include "_RFrame.h"
#include "_RNode.h"

template<typename _T>
class _RTree{
    public:
        typedef _T T;
        typedef _RTree<T> self;
        typedef _RFrame<self> RFrame;
        typedef _RNode<self> RNode;

    private:
        RNode * __p_root = nullptr;
        size_t __M; // maximum number of entries per _RNode
        size_t __m; // minimun number of entries per _RNode

        bool __f_search_private(RFrame * __frame, RNode **& __pp_node)
        {
            while((*__pp_node)->__f_get_type() == NODES)
            {

            }
            return true;
        }

    public:
        _RTree(size_t __M)
        {
            this->__M = __M;
            this->__m = this->__M >> 1;
        }

        bool __f_insert(T & __data, RFrame * __frame)
        {
            if(!__p_root)
                __p_root = new RNode(this->__M, LEAVES);

            RNode ** __pp_node = &__p_root;
            __f_search_private(__frame, __pp_node);
            if((*__pp_node)->__f_get_size() < __M)
            {
                //(*__pp_node)->__f_insert(__data,l);
                return true;
            }

            return true;
        }                

        bool f_search(T & __data)
        {

            return true;
        }
};


#endif // _RTREE_H
