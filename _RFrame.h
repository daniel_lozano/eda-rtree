#ifndef _RFRAME_H
#define _RFRAME_H

#include "includes.h"

template<typename C>
class _RFrame
{
    public:
        typedef  _RFrame<C> RFrame;
        typedef typename C::T T;
        typedef typename C::RNode RNode;

    private:
        td_p_reg __register = 0;
        RNode * __p_nodes = nullptr;
        size_t __size = 0,
               __max_size=0;
        td_coordinate __x0 = 0,
                      __y0 = 0,
                      __x1 = 0,
                      __y1 = 0;

    public:
        _RFrame(td_coordinate __x0, td_coordinate __y0,
                td_coordinate __x1, td_coordinate __y1,
                size_t __max_size=0)
        {
            this->__max_size = __max_size;
            this->__x0 = __x0;
            this->__y0 = __y0;
            this->__x1 = __x1;
            this->__y1 = __y1;
            __p_nodes = new RNode [this->__max_size];
        }

        size_t __f_size()
        {
            return __size;
        }

        td_coordinate __f_calculate_area()
        {
            td_coordinate __temp= (__x1 - __x0) * (__y1 - __y0);
            return __temp>0?__temp:-1*__temp;
        }

        td_coordinate __f_calculate_distance(RFrame * __frame)
        {
            return sqrt((__x1 - __x0) * (__x1 - __x0) +
                        (__y1 - __y0) * (__y1 - __y0));
        }

};

#endif // _RFRAME_H
